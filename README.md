# LLVM/Clang C++11 on Summit

This repo contains a reproducer that demonstrates a method to use a C++11
capable GCC toolchain with the LLVM/Clang9 build on the OLCF's Summit
supercomputer.

LLVM on Summit was compiled with the OS (RHEL7) provided GCC v4.8.5 which lacks
C++11 support. Simply passing `--gcc-toolchain=/path/to/alternate/gcc` to clang
is insufficient to swap out the GCC toolchain used due, at least in part, to
variations in runtime environment variables and loaded environment modules and
possibly also GCC and LLVM build-time configuration options that could be
improved.

The example in this repo passes the appropriate linking and header arguments to
the `clang++` incantation directly. This is generally inconvenient and can
probably be made easier by passing them via environment variables.

The author is looking into the feasibility of providing a set of
(yet-to-be-created) modulefiles that would set baseline preferences for
`CFLAGS`, `CPATH`, `C_INCLUDE_PATH`, `LIBRARY_PATH`, and other variables to
ensure proper linking and RPATHs when using LLVM on Summit with an alternate GCC
toolchain when an LLVM modulefile on Summit is loaded. However, this example
shows the only method that will always work regardless of changes made by OLCF
staff to Summit environment modules in order to be as generally useful as
possible.

# Usage

This example should be fully self-contained. Simply clone the repo into your
home directory on Summit, inspect the contents of `build.sh` for the process
involved (and just because you should never execute random shell scripts without
inspecting them first).

To test different GCC toolchains, edit the `gcc_prefix` variable in `build.sh`
to match the value of `OLCF_GCC_ROOT` on the desired GCC toolchain as taken from
the output of

```
# Replace 9.1.0 with your GCC version of choice
module show gcc/9.1.0
```

When convinced it's safe, simply run `./build.sh` which will
- setup the environment
- extract the necessary paths from the given GCC toolchain
- print the extra arguments that will be passed to `clang++`
- compile the test verbosely
- generate (or re-use an existing) input file to the test program
- run the test
- print the linking information of the output binary.
