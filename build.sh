#!/bin/bash

# Abort execution on first error
set -e

# Setup the environment
module load cuda/10.1.168 llvm/9.0.0-2

# FIXME: Declare the absolute path to a non-standard GCC toolchain for
# LLVM/Clang++ to use instead of the standard-location OS-provided ancient
# default. Feel free to try more than just one, if so inclined.

gcc_prefix="/sw/summit/gcc/6.4.0"
# gcc_prefix="/sw/summit/gcc/9.1.0-alpha+20190716"

# Get all the runtime libraries, gcc libraries, and headers needed by a
# gcc toolchain located in a nonstandard location.
gcc_version="$(${gcc_prefix}/bin/gcc -v 2>&1 | grep 'gcc version' | awk '{print $3}')"
target="$(${gcc_prefix}/bin/gcc -v 2>&1 | grep 'Target:' | awk '{print $2}')"
gcc_libs="${gcc_prefix}/lib/gcc/$target/$gcc_version"
gcc_runtime_libs="${gcc_prefix}/lib64"
gcc_headers="${gcc_prefix}/include/c++/$gcc_version"
gcc_target_headers="${gcc_prefix}/include/c++/$gcc_version/$target"

echo "-I$gcc_headers"
echo "-I$gcc_target_headers"
echo "-L$gcc_runtime_libs"
echo "-Wl,-rpath,$gcc_runtime_libs"
echo "-L$gcc_libs"

# Start with a clean build directory.
rm -f test_binary

# Tell clang we're using the GCC toolchain located in the non-standard location.
export COMPILER_PATH="${gcc_prefix}"

# FIXME: `clang++` is passing these objects to `ld` with the wrong absolute path
# when using GCC 6.4.0; other GCC builds such as 9.1.0 appear to pass the right
# information to `ld`.
ln -sf $gcc_libs/crtbegin.o
ln -sf $gcc_libs/crtend.o

# Build the test program
clang++ \
  -v -std=c++11 \
  --gcc-toolchain="${gcc_prefix}" \
  -I $gcc_headers \
  -I $gcc_target_headers \
  -L $gcc_runtime_libs \
  -Wl,-rpath,$gcc_runtime_libs \
  -L $gcc_libs \
  -o test_binary \
  test.cpp

# Run a test of the program
printf "\nRunning test using 'test.in' file with contents:\n======\n"
if [ -e 'test.in' ]; then
  # Use the existing 'test.in' file.
  cat test.in
else
  # Write a `test.in` file for the program to process.
  tee test.in <<EOF
next_var_must_have = a RHS value that can be cast to a double.
tolerance  = 123.456
blah = not_matched
EOF
fi
printf "======\n\nResult:\n"
./test_binary

# Success!
printf "\nVerifying output of 'ldd' and 'readelf -d' should show the program is linked as expected.\n\n"
ldd test_binary
